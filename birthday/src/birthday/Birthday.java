/*
 * Author: Ryan Hendrickson
 * Date: April 4th, 2019.
 * Purpose: Allows the user to input dates in a (n n) format;
 * ask for their birthday, then wishes them happy birthday
 * or tells them how many days until their birthday.
 */

package birthday;

import java.util.Scanner;

public class Birthday {
	public static void main(String[] args) {
		// make our scanner
		Scanner kb = new Scanner(System.in);
		
		// gather todays date and make a Date object called "today"
		System.out.print("What is today's date (month and day)? ");
		int month = kb.nextInt();
		int day = kb.nextInt();
		Date today = new Date(month, day);
		
		// gather the users birthday information and make a Date object
		System.out.print("What is your birthday (month and day)? ");
		month = kb.nextInt();
		day = kb.nextInt();
		Date birthday = new Date(month, day);
		
		// calculate how many days are in their birth month and print it out
		int daysInBirthdayMonth = birthday.daysInMonth();
		System.out.print("There are ");
		System.out.print(daysInBirthdayMonth);
		System.out.print(" days in month #");
		System.out.println(birthday.getMonth());
		
		// check if it's their birthday, if it is wish them a happy birthday
		// if it's not their birthday, print how long until their birthday
		if(today.equals(birthday)) {
			System.out.println("Happy birthday!");
		} else {
			int count = 0;
			while(!today.equals(birthday)) {
				today.nextDay();
				count++;
			}
			System.out.println("There are " + count + " days until your birthday!");
		}
		
		kb.close();
	}
}