/*
 * Author: Ryan Hendrickson
 * Date: June 7th, 2019.
 * Filename: RecursiveBasics.java
 * Purpose: Creates 4 methods that demonstrated how recursion works and that I know how to do it.
 */

public class RecursiveBasics {
	
	public static void main(String[] args) {
		
		writeNums(5);
		System.out.println();
		countdown(5);
		System.out.println();
		System.out.println(multiplyEvens(2));
		System.out.println(multiplyOdds(4));
	}
	
	/**
	 * prints from 1 to n
	 * @param n
	 */
	public static void writeNums(int n) {
	    if(n < 1) {
	        throw new IllegalArgumentException();
	    } else if(n == 1) {
	        System.out.print("1");
	    } else {
	        writeNums(n - 1);
	        System.out.print(", " + n);
	    }
	}
	
	/**
	 * prints from n to 1
	 * @param n
	 */
	public static void countdown(int n) {
		if(n < 1) {
			throw new IllegalArgumentException();
		} else if(n == 1) {
			System.out.print(n);
		} else {
			System.out.print(n + ", ");
			countdown(n-1);
		}
	}
	
	/**
	 * @param n
	 * @return the product of the first n even integers
	 */
	public static int multiplyEvens(int n) {
	    if (n < 1) {
	        throw new IllegalArgumentException();
	    } else if (n == 1) {
	        return 2;
	    } else {
	        return 2 * n * multiplyEvens(n - 1);
	    }
	}

	
	/**
	 * @param n
	 * @return the product of the first n odd integers
	 */
	public static int multiplyOdds(int n) {
	    if (n < 1) {
	        throw new IllegalArgumentException();
	    } else if (n == 1) {
	        return 1;
	    } else {
	    	return  1 * (n+(n-1)) * multiplyOdds(n-1);
	    }
	}
}
