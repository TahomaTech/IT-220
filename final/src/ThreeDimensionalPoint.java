/*
 * Author: Ryan Hendrickson
 * Date: June 7th, 2019.
 * Filename: ThreeDimensionalPoint
 * Purpose: Expands on the built in Java Point class by creating a z coordinate, thus allowing us to make three dimensional points.
 */
import java.awt.*;

public class ThreeDimensionalPoint extends Point implements Comparable<ThreeDimensionalPoint>{
	// We get an error/warning telling us to add this???
	private static final long serialVersionUID = 1L;
	
	// fields
	private int z;
	
	// constructors
	public ThreeDimensionalPoint(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public ThreeDimensionalPoint(int x, int y) {
		this(x, y, 0);
	}
	
	public ThreeDimensionalPoint() {
		this(0, 0, 0);
	}
		
	// getter and setter for our z coordinate
	public int getZ() {
		return this.z;
	}
	
	public void setZ(int z) {
		this.z = z;
	}
	
	// methods
	
	/**
	 * Shift the ThreeDimensionalPoint by adding dx to x, dy to y, and dz to z
	 * example: [x=1, y=1, z=1] being translated by [dx=2, dy=1, dz=3] becomes [x=3, y=2, z=4]
	 * @param dx | gets added to x
	 * @param dy | gets added to y
	 * @param dz | gets added to z
	 */
	public void translate(int dx, int dy, int dz) {
		this.x += dx;
		this.y += dy;
		this.z += dz;
	}
	
	/**
	 * override the Point class toString so it includes our z position
	 */
	public String toString() {
		return "[x=" + this.x + ", y=" + this.y + ", z=" + this.z + "]";
	}
	
	/**
	 * override our equals method
	 * first we check to see if the user provided the correct ThreeDimensionalPoint
	 * if they did we cast the obj to be a ThreeDimensionalPoint and then compare their values
	 * if they didn't, we return false
	 * @param obj
	 */
	public boolean equals(Object obj) {
		if (obj instanceof ThreeDimensionalPoint) {
			ThreeDimensionalPoint other = (ThreeDimensionalPoint) obj;
			return x == other.x && y == other.y && this.z == other.z;
		} else {  // not a Point object
			return false;
		}
	}

	/**
	 * allows the user to compare two ThreeDimensionalPoints
	 * returns a negative int if this point is smaller than the other one
	 * returns a zero if the points are equal
	 * returns a positive int if this point is larger than the other one
	 * @param other
	 */
	public int compareTo(ThreeDimensionalPoint other) {
		if(this.z < other.z) {
			return -1;
		} else if(this.z > other.z) {
			return 1;
		} else if(this.y < other.y) { // our z's are equal
			return -2;
		} else if(this.y > other.y) {
			return 2;
		} else if(this.x < other.x) { // our z's and y's are equal
			return -3;
		} else if(this.x > other.x) {
			return 3;
		} else { // everything is equal!
			return 0;
		}
	}
}
