/*
 * Author: Ryan Hendrickson
 * Date: June 10th, 2019.
 * Filename: Problem4A.java
 * Purpose: Final part of the final. Chose option A, we need to make a mirror() method that mirrors a stack
 * like so: bottom [1, 3, 2, 7] top --> bottom [1, 3, 2, 7, 7, 2, 3, 1] top
 */

// IT 220 Programming 2
// Problem 4A - Stacks and Queues

import java.util.*;

public class Problem4A {

	public static void main(String[] args) {
		testExampleFromInstructions();
		testEmptyStack();
	}

	/**
	 * takes a Stack<Integer> as a parameter and doubles the size of the Stack by
	 * appending the mirror image of the original sequence at the top of the Stack
	 * We do use a Queue<Integer> as auxiliary storage to help us move everything around
	 * @param s
	 */
	public static void mirror(Stack<Integer> s) {
		Queue<Integer> q = new LinkedList<Integer>();
		
		// See if our stack is null, if not pop() everything off and add it to the queue
		// then we get the queue's original size to keep track of moving everything around
		if(s == null) {
			throw new IllegalArgumentException();
		}
		while(!s.isEmpty()) {
			q.add(s.pop());
		}
		int queueSize = q.size();
		
		// now we just move everything around so that it's in the right order
		for(int i = 0; i < queueSize; i++) {
			int n = q.remove();
			q.add(n);
			s.push(n);
		}
		while(!s.isEmpty()) {
			q.add(s.pop());
		}
		for(int i = 0; i < queueSize; i++) {
			q.add(q.remove());
		}
		while(!q.isEmpty()) {
			s.push(q.remove());
		}
	}

	// Tests the example shown in the instructions.
	// Returns true if it works, false if it doesn't.
	public static void testExampleFromInstructions() {
		// set up the starting stack
		Stack<Integer> s = new Stack<Integer>();
		s.push(1);
		s.push(3);
		s.push(2);
		s.push(7);

		// print state before mirror
		System.out.println("before mirror: " + s.toString());

		// call mirror
		mirror(s);

		// check the results
		System.out.println("after mirror: " + s.toString());

	}

	public static void testEmptyStack() {
		// set up the starting stack
		Stack<Integer> s = new Stack<Integer>();

		// print state before mirror
		System.out.println("before mirror: " + s.toString());

		// call mirror
		mirror(s);

		// check the results
		System.out.println("after mirror: " + s.toString());
	}

} // end class Problem4A
