package midterm;

public class LineTest {
	public static void main(String[] args) {
		// test to see if we can make lines
		Line a = new Line(0, 0, 3, 4);
		Point p1 = new Point(2, 5);
		Point p2 = new Point(5, 8);
		Line b = new Line(p1, p2);
		System.out.println(a);
		System.out.println(b);
		
		// make sure our point getters work
		System.out.println(a.getP1());
		System.out.println(a.getP2());
		System.out.println(b.getP1());
		System.out.println(b.getP2());
		
		// test equals, delta, and slope
		System.out.println(a.equals(b));
		System.out.println(a.deltaX());
		System.out.println(a.deltaY());
		System.out.println(a.slope());
		System.out.println(b.slope());
	}
}
