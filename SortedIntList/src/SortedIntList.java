/**
 * @author Ryan Hendrickson
 * @date April 23rd, 2019.
 * @filename SortedIntList.java
 * @purpose expands on the built in Array class and keep an Array sorted at all times.
 */

import java.util.*;

public class SortedIntList {
	// fields
	private int[] elements;
	private int size;
	private final static int DEFAULT_CAPACITY = 100;
	private boolean unique = false;
	
	// start all of our constructors
	public SortedIntList(boolean unique, int cap) {
		if(cap < 0) {
			throw new IllegalArgumentException("Capacity can't be less than 0: " + cap);
		}
		this.unique = unique;
		elements = new int[cap];
	}
	
	public SortedIntList(int cap) {
		this(false, cap);
	}
	
	public SortedIntList(boolean unique) {
		this(unique, DEFAULT_CAPACITY);
	}
	
	public SortedIntList() {
		this(false, DEFAULT_CAPACITY);
	}
	
	// methods
	
	// get the minimum value
	public int min() {
		if(size == 0) {
			throw new NoSuchElementException("Your array is empty " + size);
		} else {
			return elements[0];
		}
	}
	
	// get the maximum value
	public int max() {
		if(size == 0) {
			throw new NoSuchElementException("Your array is empty " + size);
		} else {
			return elements[size-1];
		}
	}
	
	// get the status of our unique variable
	public boolean getUnique() {
		return unique;
	}
	
	// allow the user to change the unique variable
	// this will also clear any duplicates if it is false and then set to true
	public void setUnique(boolean unique) {
		if(unique) {
			int i = 0;
			while(i < size) {
				if(elements[i] == elements[i+1]) {
					remove(i+1);
				} else {
					i++;
				}
			}
		}
		this.unique = unique;
	}
	
	// returns the current size, or number of elements in the list
	public int size() {
		return size;
	}
	
	// throws an IndexOutOfBoundsException if the index provided is not valid
	public void checkIndex(int i) {
		if(i < 0 || i >= size) {
			throw new IndexOutOfBoundsException("Index " + i);
		}
	}
	
	// return the int at the provided index
	public int get(int i) {
		checkIndex(i);
		return elements[i];
	}

	
	// returns the position of the first occurrence of the given value
	// (-1 if not found)
	public int indexOf(int value) {
		int result = Arrays.binarySearch(elements, 0, size, value);
		return result;
	}
	
	// see if our array is empty
	public boolean isEmpty() {
		if(size == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	// see if the given value is in our array or not
	public boolean contains(int value) {
		return indexOf(value) >= 0;
	}
	
	// adds the given value to where it should be to keep the list ordered
	// we use our private add method below to make this work
	public void add(int value) {
		ensureCapacity(size+1);
		int index = indexOf(value);
		int insert = Math.abs(index+1);
		if(unique) {
			if(index < 0) {
				add(insert, value);
			}
		} else {
			add(insert, value);
		}
	}
	
	// made this method private so it can help keep our public
	// add method cleaner and work properly
	private void add(int index, int value) {
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException("Index: " + index);
		}
		for(int i = size; i > index; i--) {
			elements[i] = elements[i-1];
		}
		elements[index] = value;
		size++;
	}
	
	// ensures our array is big enough, if not it makes a new array
	// that is double the size (or more if the given capacity is larger)
	// and copies everything over
	public void ensureCapacity(int cap) {
		if(cap > elements.length) {
			int newCap = elements.length * 2 + 1;
			if(cap > newCap) {
				newCap = cap;
			}
			elements = Arrays.copyOf(elements, newCap);
		}
	}
	
	// set size to 0 and empty our array
	public void clear() {
		size = 0;
		for(int i = 0; i < size; i++) {
			elements[i] = 0;
		}
	}
	
	public void remove(int index) {
		checkIndex(index);
		for(int i = index; i < size-1; i++) {
			elements[i] = elements[i+1];
		}
		size--;
	}
	
	// Override toString
	public String toString() {
		if(size == 0 ) {
			return "[]";
		} else {
			String result = "[" + elements[0];
			for(int i = 1; i < size; i++) {
				result += ", " + elements[i];
			}
			result += "]";
			return result;
		}
	}
}
























