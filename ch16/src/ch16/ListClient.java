package ch16;

public class ListClient {
	public static void main(String[] args) {
		System.out.println("Tester for LinkedIntList");
		
		LinkedIntList list1 = new LinkedIntList();
		
		list1.add(8);
		list1.add(9);
		list1.add(1);
		list1.add(8);
		list1.add(9);
		list1.add(1);
		System.out.println(list1);
		
		// test out our new methods from 16.2/16.3
		
		// test set
		list1.set(0, 42);
		System.out.println("Setting index 0 to 42... " + list1);
		
		// test min
		System.out.println("Min value in the list is: " + list1.min());
		
		// test isSorted
		System.out.println("The list is sorted: " + list1.isSorted());
		
		// test lastIndexOf
		System.out.println("The last index of 8 is: " + list1.lastIndexOf(8));
		
		// test deleteBack
		System.out.println("Deleting: " + list1.deleteBack());
		System.out.println(list1);
		
		// test strecth
		list1.stretch(2);
		System.out.println("The new stretched list: " + list1);
		
		// test compress
		list1.compress(2);
		System.out.println("Compressed the list every 2 items: " + list1);
		
		// test transferFrom
		LinkedIntList list2 = new LinkedIntList();
		list2.add(38);
		list2.add(32);
		list2.add(12);
		System.out.println("list2 values: " + list2);
		list1.transferFrom(list2);
		System.out.println("New list1 values: " + list1);
		
		// test removeAll
		list1.removeAll(2);
		System.out.println("Removed all values of 2: " + list1);
		
		// test equals2
		System.out.println("list1 equals list2: " + list1.equals2(list2));
		
		// test removeRange
		list1.removeRange(0, 1);
		System.out.println("Deleted index 0 through 1 " + list1);
		
		// test doubleList
		list1.doubleList();
		System.out.println("list1 has been doubled: " + list1);
		
		// test rotate
		list1.rotate();
		System.out.println("list1's first value has been rotated: " + list1);
		
		// test reverse
		list1.reverse();
		System.out.println("Reversed list1: " + list1);
	}
}
