package ch16;

/**
 * @author Ryan Hendrickson
 * @filename LinkedIntList.java
 * @purpose utilizes our ListNode.java and lets us manipulate it easier by creating a list for them.
 */
import java.util.*;

public class LinkedIntList {
	// fields
	private ListNode front;
	
	// constructors | empty -> 
	public LinkedIntList() {
		front = null;
	}
	
	// methods
	
	/**
	 * returns a reference to the node at the given index
	 * @param index
	 * @return
	 */
	private ListNode nodeAt(int index) {
		ListNode current = front;
		for(int i = 0; i < index; i++) {
			current = current.next;
		}
		return current;
	}
	
	/**
	 * @param index
	 * @return the int data at the given index in the list
	 */
	public int get(int index) {
		ListNode current = nodeAt(index);
		return current.data;
	}
	
	/**
	 * calculates the index of the given node
	 * @param value
	 * @return 0
	 * not sure what went wrong but somewhere along the way the actual method got deleted
	 */
	public int indexOf(int value) {
		return 0;
	}
	
	/**
	 * @return the current number of elements in the list
	 */
	public int size() {
		int count = 1;
		ListNode current = front;
		while(current.next != null) {
			current = current.next;
			count++;
		}
		return count;
	}
	
	/**
	 * removes the node at the given index
	 * @param index
	 */
	public void remove(int index) {
		if(index == 0) {
			front = front.next;
		} else {
			ListNode current = nodeAt(index-1);
			current.next = current.next.next;
		}
	}
	
	/**
	 * add a ListNode to the end of our list
	 */
	public void add(int value) {
		if(front == null) {
			front = new ListNode(value);
		} else {
			ListNode current = front;
			while(current.next != null) {
				current = current.next;
			}
			current.next = new ListNode(value);
		}
	}
	
	/**
	 * allows the user to enter a value at whichever index they want
	 * @param index
	 * @param value
	 */
	public void add(int index, int value) {
		if(index == 0) { // adding at the front
			front = new ListNode(value, front);
		} else { // adding in the middle somewhere
			ListNode current = nodeAt(index-1);
			current.next = new ListNode(value, current.next);
		}
	}
	
	/**
	 * return a comma separated, bracketed list like:
	 * [] for an empty list
	 * [4, 5, 2, 1, 9] for a non-empty list
	 */
	public String toString() {
		if(front == null) {
			return "[]";
		} else {
			String result = "[" + front.data;
			ListNode current = front.next;
			while(current != null) {
				result += ", " + current.data;
				current = current.next;
			}
			result += "]";
			return result;
		}
	}
	
	// START OF NEW METHODS==============================
	/**
	 * allows the user to set the node at the given index to the given value
	 * @param index
	 * @param value
	 */
	public void set(int index, int value) {
	    ListNode temp = front;
	    for(int i = 0; i < index; i++) {
	        temp = temp.next;
	    }
	    temp.data = value;
	}
	
	/**
	 * allows the user to find the minimum value that is stored in the list
	 * @return n
	 */
	public int min() {
	    if(front == null) {
	        throw new NoSuchElementException();
	    } else {
	        int n = front.data;
	        ListNode temp = front;
	        while(temp != null) {
	            if(temp.data < n) {
	                n = temp.data;
	            }
	            temp = temp.next;
	        }
	    return n;
	    }
	}
	
	/**
	 * allows the user to see if their list is sorted from smallest to largest
	 * @return false or true depending on if the list is sorted
	 */
	public boolean isSorted() {
	    ListNode temp = front;
	    if(temp == null) {
	        return true;
	    }
	    while(temp.next != null) {
	        ListNode current = temp.next;
	        if(current.data < temp.data) {
	            return false;
	        }
	        temp = current;
	    }
	    return true;
	}
	
	/**
	 * allows the user to see where the last index of the given parameter is
	 * if the parameter isn't in the list, return -1
	 * @param i
	 * @return n
	 */
	public int lastIndexOf(int i) {
	    int n = -1;
	    int count = 0;
	    ListNode temp = front;
	    while(temp != null) {
	        if(temp.data == i) {
	            n = count;
	        }
	        count++;
	        temp = temp.next;
	    }
	    return n;
	}
	
	/**
	 * allows the user to delete the last item in their list and returns the value (i)
	 * @return i
	 */
	public int deleteBack() {
	    ListNode temp = front;
	    int i = 0;
	    if(temp == null) {
	        throw new NoSuchElementException();
	    } else if (temp.next == null) {
	        i = temp.data;
	        front = null;
	        return i;
	    } else {
	        while(temp.next.next != null) {
	            temp = temp.next;
	        }
	        i = temp.next.data;
	        temp.next = null;
	        return i;
	    }
	}
	
	/**
	 * allows the user to provide an integer (n) as a parameter and that increases a list of integers
	 * by a factor of n by replacing each integer in the original list with n copies of that integer
	 * @param n
	 */
	public void stretch(int n) {
	    if(n <= 0) {
	        front = null;
	    } else {
	        ListNode temp = front;
	        while(temp != null) {
	            int d = temp.data;
	            for(int i = 1; i < n; i++) {
	                temp.next = new ListNode(d, temp.next);
	                temp = temp.next;
	            }
	            temp = temp.next;
	        }
	    }
	}
	
	/**
	 * allows the user to provide an integer (n) representing a "compression factor" and
	 * replaces every n elements with a single element whose data value is the sum of those n nodes
	 * @param comp
	 */
	public void compress(int comp) {
	    ListNode temp = front;
	    while (temp != null) {
	        for (int i = 1; i < comp; i++) {
	            if (temp.next != null) {
	                temp.data += temp.next.data;
	                temp.next = temp.next.next;
	            }
	        }
	        temp = temp.next;
	    }
	}
	
	/**
	 * allows the user to provide a LinkedIntList (l) that this method will copy
	 * the contents from and add them to the list it was called upon
	 * @param l
	 */
	public void transferFrom(LinkedIntList l) {
	    if(front == null) {
	        front = l.front;
	    } else {
	        ListNode temp = front;
	        while(temp.next != null) {
	            temp = temp.next;
	        }
	    temp.next = l.front;
	    }
	    l.front = null;
	}
	
	/**
	 * allows the user to remove all occurrences of a particular value
	 * @param rem
	 */
	public void removeAll(int rem) {
	    while(front != null && front.data == rem) {
	        front = front.next;
	    }
	    ListNode temp = front;
	    while(temp != null && temp.next != null) {
	        if(temp.next.data == rem) {
	            temp.next = temp.next.next;
	        } else {
	            temp = temp.next;
	        }
	    }
	}
	
	/**
	 * allows the user to provide a second list as a parameter and
	 * that returns true if the two lists are equal and that returns false otherwise
	 * @param l
	 * @return
	 */
	public boolean equals2(LinkedIntList l) {
	    ListNode temp1 = front;
	    ListNode temp2 = l.front;
	    while(temp1 != null && temp2 != null) {
	        if(temp1.data != temp2.data) {
	            return false;
	        }
	        temp1 = temp1.next;
	        temp2 = temp2.next;
	    }
	    return temp1 == null && temp2 == null;
	}
	
	/**
	 * allows the user to provides a starting and ending index as parameters
	 * and removes the elements at those indexes (inclusive) from the list
	 * @param start
	 * @param stop
	 */
	public void removeRange(int start, int stop) {
	    if(start < 0 || stop < 0) {
	        throw new IllegalArgumentException();
	    } else {
	        ListNode startNode = front;
	        for(int i = 0; i < start-1; i++) {
	            startNode = startNode.next;
	        }
	        ListNode endNode = front;
	        for(int i = 0; i < stop; i++) {
	            endNode = endNode.next;
	        }
	        if(start == 0) {
	            front = endNode.next;
	        }
	        startNode.next = endNode.next;
	    }
	}
	
	/**
	 * allows the user to double the size of a list by appending a copy of the original sequence to the end of the list
	 */
	public void doubleList() {
	    if(front != null) {
	        ListNode temp = front;
	        ListNode end = front;
	        int count = 1;
	        while(end.next != null) {
	            count++;
	            end = end.next;
	        }
	        while(count > 0) {
	            count--;
	            end.next = new ListNode(temp.data);
	            temp = temp.next;
	            end = end.next;
	        }
	    }
	}
	
	/**
	 * allows the user to move the value at the front of a list of integers to the end of the list
	 */
	public void rotate() {
	    if(front != null && front.next != null) {
	        ListNode first = front;
	        front = front.next;
	        first.next = null;
	        ListNode temp = front;
	        while(temp.next != null) {
	            temp = temp.next;
	        }
	        temp.next = first;
	    }
	}
	
	/**
	 * allows the user to reverse the order of the elements in the list
	 */
	public void reverse() {
	    LinkedIntList list = new LinkedIntList();
	    if (front != null) {
	        ListNode current = front.next;
	        front.next = null;
	        list.front = front;
	        while (current != null) {
	            ListNode temp = current;
	            current = current.next;
	            temp.next = list.front;
	            list.front = temp;
	        }
	    }
	    front = list.front;
	}
	// END OF NEW METHODS================================
}
