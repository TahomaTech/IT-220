% Java Notes
% Ryan Hendrickson

# Variables:
## Description:
named storage location in memory.

## Needs:
- name
- type
- value
- location - address (in memory)

---

# Variable types:
## Primitive types:
- int
- double
- float
- char
- boolean

## Reference types:(class types)
- String
- Array
- Scanner
- Object

---

# Objects
By declaring a new object in memory, the following happens:

Scanner in = new Scanner(System.in)

Variable "in" will reference the memory location that our scanner is at.
The object won't have a name! You can only reference it via memory address.

---

# Chapter 8
## Objects have:
- State (date) - properties [fields/attributes]
- Behavior (methods) - actions [methods]

## Clients are:
- Program that uses objects

---

# Chapter 9
## Inheritance:
We are building inheritance hierarchies by extending classes into other classes. Also called type hierachy/family.

## Polymorphism:
Multiple forms/shapes - refers to overriding things

## Example:
Parent class:

- Animal =  Super class

Child classes: 

- Lion/Cow = Sub class

'is-a' relationship - lion is an animal, cow is an animal, etc.

Extends:

- a class inherits from...
- a class derived from...


## Overriding a method:
- Child class overrides parent class
- Only works if same everything
   - ie: visibility, return type, name, parameters, etc.

*Every class will extend the "Object" class by default*

**Static** makes it so the variable/function/method isn't stored in each individual object and is instead stored in memory on its own, allowing each object that needs it to just call on it when it needs to.

*super* goes to the *parent* class and overrides local variables and methods.

- Useful for extending classes and not using the setters.

---

# Stacks and Queues


---

# Recursion
*Practice with stacks and queues.*

Every method will have **two** ways to repeat; loops, and *recursion*.

The method will call **itself** until the stack is complete. Once the stack is complete, the recursion will cause the method to complete, move up on the stack, finishing that method, causing it to move up the stack, and repeat this process until the original method call is finished.

*Base case*: where you want ot stop, where is the smallest thing you can solve.

*Recursive case*: anything larger than 1, I want call myself; but in smaller terms.

---

# Trees

- **Directed**: Has one-war links between nodes.
- **Acyclic**: No path wraps back around to the same node twice.
- **Binary tree**: One where each node has at most two children.

A tree can be defined as either:

- Empty(null)
- a **root** node that contains: data, a **left** subtree, and a **right** subtree.

## Terminology:

- **Node**: an object containing a data value and a left/right child.
- **Root**: 

- **Subtree**: a tree of nodes reachable to the left/right from the current one.
- **Height**: length of the longest path from the root to any node.
- **Level or Depth**: length of the path from a root to a given node.
- **Full tree**: one where every branch has 2 children.

## Traversals
An examination of the elements of a tree.

- A pattern used in many tree algorithms and methods.

Common orderings for traversals:

- **Pre-order**: process root node, then its left/right subtrees.
- **In-order**: process left subtree, then root node, then right.
- **Post-order**: process left/right subtrees then root node.
