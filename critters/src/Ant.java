/**
 * @author Ryan Hendrickson
 * @date Wednesday, April 17th, 2019.
 * @filename Ant.java
 * @purpose create our ant class for the critters assignment.
 */
import java.awt.*;

public class Ant extends Critter {
	// fields
	private boolean walkSouth;
	private int timesMoved = 1;
	
	// constructor
	public Ant(boolean walkSouth) {
		this.walkSouth = walkSouth;
	}
	
	// start overriding our critter methods
	public Color getColor() {
		return Color.RED;
	}
	
	public boolean eat() {
		return true;
	}
	
	public Attack fight(String opponent) {
		return Attack.SCRATCH;
	}
	
	public Direction getMove() {
		if(walkSouth) {
			timesMoved++;
			if(timesMoved % 2 != 0) {
				return Direction.SOUTH;
			} else {
				return Direction.EAST;
			}
		} else { // else north/east walk
			return Direction.NORTH;
		}
	}
	
	public String toString() {
		return "%";
	}
}
