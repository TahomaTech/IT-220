/**
 * @author Ryan Hendrickson
 * @date Wednesday, April 19th, 2019.
 * @filename Vulture.java
 * @purpose create our vulture class for the critters assignment.
 */

import java.awt.Color;

public class Vulture extends Bird {
	// fields
	private boolean hungry = true;
	
	// constructor
	public Vulture() {
		
	}
	
	public Color getColor() {
		return Color.BLACK;
	}
	
	public boolean eat() {
		if(hungry) {
			hungry = false;
			return true;
		} else {
			return false;
		}
	}
	
	public Attack fight(String opponent) {
		// if it's an ant, we roar
		if(opponent.equalsIgnoreCase("%")) {
			hungry = true;
			return Attack.ROAR;
		} else {
			hungry = true;
			return Attack.POUNCE;
		}
	}
}
