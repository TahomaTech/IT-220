
public class SimpleTest {
	public static void main(String[] args) {
		ArrayIntList ail = new ArrayIntList();
		ail.add(37);
		ail.add(86);
		ail.add(25);
		ail.add(3);
		ail.add(94);
		System.out.println(ail.toString());
		
		ail.add(53);
		System.out.println(ail.toString());
		
		System.out.println("The index of 25 is: " + ail.indexOf(25));

		ail.remove(ail.indexOf(25));
		System.out.println(ail.toString());
		
		ail.add(1, 17);
		System.out.println(ail.toString());
		
		// set result to -1, try to get the int at index -10
		// -10 can't be a thing, so the program catches the error
		int result = -1;
		try {
			result = ail.get(-10);
		}
		catch(Exception e) {
			System.out.println("Something went wrong :(");
			System.out.println(e);
		}
		System.out.println(result);
	}
}
