import java.util.ArrayList;

/**
 * AssassinManager that keeps track of who is stalking whom and the history
 * of who killed whom by maintaining two linked lists, a list of players who
 * are currently alive in the "kill ring" and a list of players who are
 * currently dead in the "graveyard".
 *
 * @author Ryan Hendrickson
 * @version 1.0
 * @date May 23rd, 2019.
 * @purpose allows the user to create a game of 'assassination' where people (nodes) hunt down the next person (node)
 */
public class AssassinManager {

	/**
	 * killRingFront field: reference to the front node of the kill ring
	 */
	private AssassinNode killRingFront;

	/**
	 * graveyardFront field: reference to the front node of the graveyard (null if empty)
	 */
	private AssassinNode graveyardFront;

	/**
	 * Initialize a new assassin manager over the given list of people.
	 * @param names
	 * @throws IllegalArgumentException if the list is null or has a size of 0
	 */
	public AssassinManager(ArrayList<String> names) {
		// You receive a list of names (as a parameter).
		// Take the names and build up the kill ring of linked nodes that
		// contains the names in the same order as you received them in
		// the ArrayList. You may assume that the names are non-empty, non-null
		// strings and that there are no duplicates.
		// Note: you receive a list of names as strings. You need to create a new
		// AssassinNode object for each player and put their name into the node
		// and connect the nodes together into a list that killRingFront references.

		if(names.size() == 0 || names == null) {
			throw new IllegalArgumentException("The list is empty!");
		} else {
			killRingFront = new AssassinNode(names.get(0));
			AssassinNode current = killRingFront;
			for(int i = 1; i < names.size()-1; i++) {
				current.next = new AssassinNode(names.get(i));
				current = current.next;
			}
		}
	}

	/**
	 * Prints the names of the people in the kill ring, one per line, indented
	 * by two spaces, as "name is stalking name". The behavior is unspecified if
	 * the game is over.
	 */
	public void printKillRing() {
		AssassinNode current = killRingFront;
		while(current != null){
			if(current.next == null){
				System.out.println("  " + current.name + " is stalking " + killRingFront.name);
			} else {
				System.out.println("  " + current.name + " is stalking " + current.next.name);            
			}
			current = current.next;
		}
	}

	/**
	 * Prints the names of the people in the graveyard, one per line, with each
	 * line indented by two spaces, with output of the form "name was killed by
	 * name". It should print the names in the opposite of the order in which
	 * they were killed (most recently killed first, then next more recently
	 * killed, and so on). It should produce no output if the graveyard is empty.
	 */
	public void printGraveyard() {
		AssassinNode current = graveyardFront;
		while(current != null){
			if(current.next == null){
				System.out.println("  " + current.name + " was killed by " + current.killer);
			} else {
				System.out.println("  " + current.name + " was killed by " + current.killer);            
			}
			current = current.next;
		}
		System.out.println();
	}

	/**
	 * Checks to see if <code>name</code> is in the current kill ring.
	 * @param name name to check
	 * @return true if the <code>name</code> is in the kill ring and false otherwise
	 */
	public boolean killRingContains(String name) {
		AssassinNode current = killRingFront;
		while(current != null){
			if(current.name.equalsIgnoreCase(name)){
				return true;
			}
			current = current.next;
		}
		return false;
	}

	/**
	 * Checks to see if <code>name</code> is in the current graveyard.
	 * @param name name to check
	 * @return true if the <code>name</code> is in the graveyard and false otherwise
	 */
	public boolean graveyardContains(String name) {
		AssassinNode current = graveyardFront;
		while(current != null){
			if(current.name.equalsIgnoreCase(name)){
				return true;
			}
			current = current.next;
		}
		return false;
	}

	/**
	 * Checks to see if the game is over (if the kill ring has only one player
	 * remaining).
	 * @return true if the game is over and false otherwise
	 */
	public boolean isGameOver() {
		if(killRingFront.next == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Obtains the name of the winner of the game.
	 * @return name of the winner of the game or <code>null</code> if the game
	 *         is not over
	 */
	public String winner() {
		if(killRingFront.next == null) {
			return killRingFront.name;
		} else {
			return null;
		}
	}

	/**
	 * Transfers a player from the kill ring to the front of the graveyard. This
	 * operation does not change the relative order of the kill ring. Case is
	 * ignored in comparing names.
	 * @param name the name of the player to be transferred from the kill ring
	 *             to the graveyard
	 * @throws IllegalStateException if the game is over
	 * @throws IllegalArgumentException if the given name is not part of the kill ring
	 */
	public void kill(String name) {
		if(isGameOver()){
			throw new IllegalStateException();
		}
		if(!killRingContains(name)){
			throw new IllegalArgumentException("Player does not exist in Kill Ring!");
		} else {
			AssassinNode theFallen = null;

			// remove the victim from the Kill Ring
			if(killRingFront.name.equalsIgnoreCase(name)){
				// if victim is at front of kill ring
				String assassin = null;
				AssassinNode current = killRingFront;
				while(current != null){ 
					if(current.next == null){
						assassin = current.name;
					}
					current = current.next;
				}
				theFallen = killRingFront;
				theFallen.killer = assassin;
				killRingFront = killRingFront.next;

			} else {
				// if victim is not at front of kill ring
				AssassinNode current = killRingFront;
				while(current.next != null){
					if(current.next.name.equalsIgnoreCase(name)){
						String assassin = current.name;
						theFallen = current.next;
						theFallen.killer = assassin;
						if(current.next.next != null){
							// if there is someone after the victim in list
							current.next = current.next.next;
							break;
						} else {
							// if there is not someone after the victim
							current.next = null;
							break;
						}
					}
					current = current.next;
				}
			}

			// place the victim at the front of the graveyard
			if(graveyardFront != null){
				theFallen.next = graveyardFront;
			} else {
				theFallen.next = null;
			}
			graveyardFront = theFallen; 
		}
	}
}
// OUTPUT:
// had to do it, being I did this on a linux distro.
//Game was won by Linus Torvalds
//Final graveyard is as follows:
//The final graveyard is as follows:
//  John von Naumann was killed by Linus Torvalds
//  Don Knuth was killed by Linus Torvalds
//  Alan Kay was killed by Don Knuth
//  Ada Lovelace was killed by John von Naumann
//  Charles Babbage was killed by Alan Kay
//  Alan turing was killed by Don Knuth
//  Bill Gates was killed by Linus Torvalds
//  Alonzo Church was killed by Ada Lovelace
//  Tim-Berners-Lee was killed by Alan turing
