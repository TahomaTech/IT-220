package chapter_9;

public class Animal {
	// all of the common fields/properties of an Animal
	private String name;
	private int age;
	private double weight;
	
	// constructors
	public Animal() {
		
	}
	
	public Animal(String name, int age, double weight) {
		this.name = name;
		this.age = age;
		this.weight = weight;
	}
	
	// all of the common behaviors/methods
	public void eat() {
		System.out.println("I love to eat");
	}
	
	public void sleep() {
		System.out.println("zzz");
	}
	
	public void move() {
		System.out.println("Okilly Dokilly");
	}
	
	// mutators
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}

	// getters
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public double getWeight() {
		return weight;
	}
}
