package chapter_9;

public class Lion extends Animal {
	// fields/properties
	private boolean hasMane;
	
	// default constructor then constructor with parameters
	public Lion() {
		
	}
	
	public Lion(String name, int age, double weight, boolean hasMane) {
		this.setName(name);
		this.setAge(age);
		this.setWeight(weight);
		this.hasMane = hasMane;
	}
	
	// methods
	public void eat() {
		System.out.println("Yum, read meat!");
	}
	
	public String roar() {
		if(this.getAge() < 5) {
			return "rooaaarrrr";
		} else {
			return "ROOAAARRRR";
		}
	}
	
	public void move() {
		System.out.println("Let's go");
	}
	
	// accessors
	public boolean getMane() {
		return hasMane;
	}
	
	// mutators
	public void setMane(boolean hasMane) {
		this.hasMane = hasMane;
	}
}
