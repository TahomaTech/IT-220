package chapter_9;

public class AnimalTester {
	
	public static void main(String[] args) {
		Lion simba = new Lion("Simba", 1, 200, false);
		System.out.println(simba.roar());
		simba.eat();
		simba.sleep();
		System.out.println("================");
		Cow babyCow = new Cow("Bessie", 3, 99, false, "unknown");
		System.out.println(babyCow.moo());
		babyCow.eat();
		babyCow.move();
	}
}
