package chapter_9;

public class Cow extends Animal{
	// fields/attributes
	private boolean spots;
	private String familyName;
	
	// constructors
	public Cow() {
		
	}
	
	public Cow(String name, int age, double weight, boolean spots, String familyName) {
		this.setName(name);
		this.setAge(age);
		this.setWeight(weight);
		this.spots = spots;
		this.familyName = familyName;
	}
	
	// methods
	public void eat() {
		System.out.println("I love veggies");
	}
	
	public void move() {
		System.out.println("Do I have to?");
	}
	
	public String moo() {
		if(this.getWeight() < 100) {
			return "Too young to moo";
		} else {
			return "Moo!";
		}
	}
	
	// accessors
	public boolean getSpots() {
		return spots;
	}
	
	public String getFamilyName() {
		return familyName;
	}
	
	// mutators
	public void setSpots(boolean spots) {
		this.spots = spots;
	}
	
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
}
